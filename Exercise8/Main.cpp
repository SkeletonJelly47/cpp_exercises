#include <iostream>
#include <stdlib.h>
#include <string>
#include <set>

int main()
{
	bool playingGame = true;
	int players = 0;
	int currentPlayer = 0;
	std::set<int> guessedNumbers;
	std::set<int>::iterator it;
	bool playAgain = true;

	while (playAgain)
	{
		do
		{
			system("CLS");
			std::cout << "Select the amount of players (2-4): " << std::endl;
			std::cin >> players;
		} 
		while (players > 4 || players < 2);

		while (playingGame)
		{
			//Ask a number
			int number = 0;

			do
			{
				system("CLS");
				if (number < 0 || number > 20)
				{
					std::cout << "Number wasn't between 0 and 20!" << std::endl;
				}

				std::cout << "Player " << currentPlayer << " turn" << std::endl;
				std::cout << "Give a number between 0-20 that hasn't been given already: " << std::endl;
				std::cin >> number;
			}
			while (number < 0 || number > 20);

			std::set<int>::const_iterator it = guessedNumbers.find(number);
			if (it != guessedNumbers.cend())
			{
				//Game lost!
				std::cout << "Player " << currentPlayer << " lost the game by guessing the number " << number << " again!" << std::endl;
				playingGame = false;
				system("PAUSE");
				continue;
			}
			if (currentPlayer < players-1)
			{
				currentPlayer++;
			}
			else
			{
				currentPlayer = 0;
			}

			//Reset set if 20 numbers reached
			if (guessedNumbers.size() + 1 < 20)
			{
				guessedNumbers.insert(it, number);
			}
			else
			{
				guessedNumbers.clear();
			}

			system("CLS");
		}

		int selection = 0;
		do
		{
			system("CLS");
			std::cout << "Do you wan to play again?" << std::endl;
			std::cout << "0 for no, 1 for yes" << std::endl;
			std::cin >> selection;
		}
		while (selection < 0 || selection > 1);

		playAgain = (bool)selection;
		playingGame = (bool)selection;
		guessedNumbers.clear();
	}
}