#include <iostream>

//Don't use all caps ffs
struct Node
{
	int value;
	Node* next;
};

void PrintList(Node* head);
void AppendToLinkedList(Node* head, Node* value);

int main()
{
	Node start = { 0, 0 };
	Node nextNode1 = { 1, 0 };
	Node nextNode2 = { 2, 0 };
	Node nextNode3 = { 3, 0 };
	Node nextNode4 = { 4, 0 };
	Node nextNode5 = { 5, 0 };
	Node nextNode6 = { 6, 0 };

	AppendToLinkedList(&start, &nextNode1);
	AppendToLinkedList(&start, &nextNode2);
	AppendToLinkedList(&start, &nextNode3);
	AppendToLinkedList(&start, &nextNode4);
	AppendToLinkedList(&start, &nextNode5);
	AppendToLinkedList(&start, &nextNode6);

	PrintList(&start);
}

void PrintList(Node* head)
{
	//Temp variable to store the progress of our loop
	Node* current = head;

	//Print the first value before the loop
	std::cout << current->value;

	//Find the end
	do
	{
		current = current->next;
		std::cout << current->value;
	}
	while (current->next != nullptr);

	std::cout << std::endl;
}

void AppendToLinkedList(Node* head, Node* append)
{
	//Temp variable to store the progress of our loop
	Node* current = head;

	//Find the end
	while (current->next != nullptr)
	{
		current = current->next;
	}

	//Append to the list
	current->next = append;
}
