#include <iostream>

void PrintArrayContents(int integers[], int length);
void PrintArrayContents2(int integers[], int length);
void ReadUserValues();

int main()
{
	int value = 8;		//Create int with value
	int* ptr = &value;	//De-reference pointer

	int values[] = { 1, 2, 3, 4, 5, 6, 7 };

	PrintArrayContents(values, (sizeof(values)/ sizeof(values[0]))); //Wont work on all operating systems since sizeof(int) can be different
}

void PrintArrayContents(int integers[], int length)
{
	for (int i = 0; i < length; i++)
	{
		std::cout << integers[i];
	}
	std::cout << std::endl;
}

void PrintArrayContents2(int integers[], int length)
{
	//Store the pointer of that is beyond the integer array, to act as a boundary
	int* endPtr = integers + length;

	//Continue as long as the value is not at the end of the array
	for (int *value = integers; value != endPtr; value++)
	{
		std::cout << value;
	}

	std::cout << std::endl;
}

void ReadUserValues()
{

}
