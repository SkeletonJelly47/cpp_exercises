#include "Task.h"
#include <iostream>


Task::Task() : priority(Priority::Normal)
{
}

Task::Task(std::string name, std::string deadline, Priority priority)
	: name(name), deadline(deadline), priority(priority)
{

}

void Task::PrintInfo() const
{
	std::cout << "Name: " << name << std::endl;
	std::cout << "Deadline: " << deadline << std::endl;
	std::cout << "Priority: " << priority << "\n" << std::endl;
}

Task::~Task()
{
}
