#include <iostream>
#include <vector>
#include <algorithm>
#include "EvenFunctor.h"

int main()
{
	EvenFunctor ef;

	std::vector<int> integers;

	for (int i = 0; i < 30; i++)
	{
		integers.push_back(i);
	}

	std::for_each(integers.begin(), integers.end(), ef);


	system("PAUSE");


	integers.clear();

	for (int i = 0; i < 100; i++)
	{
		integers.push_back(i);
	}

	std::for_each(integers.begin(), integers.end(), [](int x) 
	{
		bool isPrime = true;
		for (int i = 2; i <= x / 2; ++i)
		{
			if (x % i == 0)
			{
				isPrime = false;
				break;
			}
		}

		if (isPrime)
		{
			std::cout << x << " Is a prime number!" << std::endl;
		}
		else
		{
			//std::cout << x << " Is not a prime number!" << std::endl;
		}
	});
	system("PAUSE");
}