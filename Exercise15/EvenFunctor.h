#pragma once
#include <iostream>

class EvenFunctor
{
public:
	
	void operator() (int n)
	{
		if (n%2 == 0)
		{
			std::cout << n << " Is even" << std::endl;
		}
		else
		{
			std::cout << n << " Is odd" << std::endl;
		}
	}
};

