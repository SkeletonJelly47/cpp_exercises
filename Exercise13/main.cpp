#include "Vector2.h"
#include <iostream>

int main()
{
	Vector2 vec0(2.0f, 4.0f);
	Vector2 vec1(6.0f, 8.0f);
	Vector2 vec2(1.0f, 0.5f);

	std::cout << vec0 + vec1 << std::endl;

	std::cout << vec0 * vec1 << std::endl;

	std::cout << vec0 - vec2 << std::endl;

	std::cout << vec0 * 5.0f << std::endl;
	
	vec1++;
	vec2--;

	std::cout << vec1 << std::endl;
	std::cout << vec2 << std::endl;

	system("PAUSE");
}