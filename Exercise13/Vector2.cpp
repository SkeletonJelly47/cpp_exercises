#include "Vector2.h"



Vector2::Vector2()
{
}

Vector2::Vector2(float x, float y) : x(x), y(y)
{
}


Vector2::~Vector2()
{
}

//Operator overloads
Vector2& Vector2::operator+=(const Vector2 vector)
{
	x += vector.x;
	y += vector.y;
	return *this;
}
Vector2& Vector2::operator-=(const Vector2 vector)
{
	x -= vector.x;
	y -= vector.y;
	return *this;
}
Vector2& Vector2::operator++()
{
	x++;
	y++;
	return *this;
}
Vector2& Vector2::operator--()
{
	x--;
	y--;
	return *this;
}
Vector2 Vector2::operator++(int)
{
	return Vector2(x++, y++);
}
Vector2 Vector2::operator--(int)
{
	return Vector2(x--, y--);
}
Vector2 operator+(Vector2& v1, Vector2& v2)
{
	return Vector2(v1.x + v2.x, v1.y + v2.y);
}
Vector2 operator-(Vector2& v1, Vector2& v2)
{
	return Vector2(v1.x - v2.x, v1.y - v2.y);
}
Vector2 operator*(Vector2& v1, Vector2& v2)
{
	return Vector2(v1.x * v2.x, v1.y * v2.y);
}
Vector2 operator*(Vector2& vector, float scalar)
{
	return Vector2(vector.x * scalar, vector.y * scalar);
}
std::ostream & operator<<(std::ostream & os, const Vector2 & vec)
{
	os << "X:" << vec.x << "f Y:" << vec.y << "f";
	return os;
}