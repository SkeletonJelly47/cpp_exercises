#pragma once
#include <iostream>

class Vector2
{
public:
	Vector2();
	Vector2(float x, float y);
	~Vector2();

	//Member operators
	Vector2& operator+=(const Vector2 vector);
	Vector2& operator-=(const Vector2 vector);
	Vector2& operator++();
	Vector2& operator--();
	Vector2 operator++(int);
	Vector2 operator--(int);
	
	//External friend operators 
	friend Vector2 operator+(Vector2 &v1, Vector2 &v2);
	friend Vector2 operator-(Vector2 &v1, Vector2 &v2);
	friend Vector2 operator*(Vector2 &v1, Vector2 &v2);
	friend Vector2 operator*(Vector2 &vector, float scalar);
	friend std::ostream& operator<<(std::ostream &os, const Vector2 &vec);

private:
	float x, y;

};

