#include "Vector.h"
#include <iostream>

MyVector::MyVector(int p_size)
	: size(p_size), count(0)
{
	list = new int[size];
}

MyVector::~MyVector()
{
	delete list;
}

void MyVector::AddItem(int item)
{
	if (count < size)
	{
		list[count] = item;
		count++;
	}
	else
	{
		allocateMore();

		list[count] = item;
		count++;
	}
}

void MyVector::allocateMore()
{
	std::cout << "Allocating more space in vector." << std::endl;

	//Old variables
	int* tempStorage = list;
	int oldSize = size;

	//Keep track of size and allocate more with new
	size += ALLOCATE_AMOUNT;
	list = new int[size];

	std::cout << "New size is: " << size << std::endl;

	//Copy old elements to new
	for (int i = 0; i < oldSize; i++)
	{
		list[i] = tempStorage[i];
	}

	delete tempStorage;
}

void MyVector::Print()
{
	for (size_t i = 0; i < count; i++)
	{
		std::cout << "Element " << i << " value is: " << list[i] << "\n";
	}
	std::cout << std::endl;
}