#include <iostream>
#include "Vector.h"

int main()
{
	MyVector justRight(10);

	for (int i = 0; i < 15; i++)
	{
		justRight.AddItem(i);
	}

	justRight.Print();

	std::cin.get();
}