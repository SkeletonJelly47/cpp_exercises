#pragma once
#include <iostream>

class MyVector
{
private:
	const int ALLOCATE_AMOUNT = 100;
	int size;
	int count;
	int* list;

	void allocateMore();

public:
	MyVector(int p_size);
	~MyVector();

	void AddItem(int value);

	void Print();
};