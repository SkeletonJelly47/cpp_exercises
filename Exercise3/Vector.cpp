#include "Vector.h"
#include <iostream>

MyVector::MyVector(int p_size) : size(p_size)
{
	if (size <= MAX_SIZE || size > 0)
	{
		this->size = size;
		this->count = 0;
		list = new int[size];
	}
	else
	{
		std::cout << "Array is too big or negative!" << std::endl;
	}
}

void MyVector::AddItem(int item)
{
	if (count + 1 > MAX_SIZE)
	{
		std::cout << "Array going to be too big" << std::endl;
		return;
	}
	list[count] = item;
	count++;
}

void MyVector::Print()
{
	for (size_t i = 0; i < count; i++)
	{
		std::cout << "Element " << i << " value is: " << list[i] << "\n";
	}
	std::cout << std::endl;
}