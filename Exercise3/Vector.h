#pragma once
#include <iostream>

class MyVector
{
public:
	const int MAX_SIZE = 10;

private:
	int size;
	int count;
	int* list;

public:
	MyVector(int p_size);

	void AddItem(int value);

	void Print();
};