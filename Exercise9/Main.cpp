#include <algorithm>
#include <list>
#include <iostream>

int GenerateNumber();
int current = 0;
int increment = 1;

int main()
{
	std::list<int> integers(20);

	/*integers.emplace_back(1);
	integers.emplace_back(2);
	integers.emplace_back(4);
	integers.emplace_back(8);
	integers.emplace_back(6);
	integers.emplace_back(30);
	integers.emplace_back(10);
	integers.emplace_back(20);
	integers.emplace_back(19);
	integers.emplace_back(25);*/

	std::list<int>::iterator iterator = integers.begin();
	std::generate_n(iterator , integers.size(), GenerateNumber);

	//std::sort(integers.begin(), integers.end());

	for (; iterator != integers.end(); iterator++)
	{
		std::cout << *iterator << std::endl;
	}
	system("PAUSE");
}

int GenerateNumber()
{
	if (current % 10 == 0)
	{
		increment *= 2;
	}
	current += increment;
	return current;
}