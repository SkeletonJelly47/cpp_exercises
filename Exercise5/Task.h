#pragma once
#include <string>

class Task
{
public:

	enum Priority {Low, Normal, Urgent};

	//Constructors
	Task();
	Task(std::string name, std::string deadline, Priority priority);

	~Task();

	void PrintInfo() const;

	std::string GetName()		{ return name; }
	std::string GetDeadline()	{ return deadline; }
	Priority GetPriority()		{ return priority; }

protected:

	std::string name;
	std::string deadline;
	Priority priority;

};
