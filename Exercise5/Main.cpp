#include <iostream>
#include <vector>
#include <stdlib.h>
#include <string>
#include "Task.h"

Task CreateTask();
void PrintTasks(const std::vector<Task>& tasks);
void DeleteTasks(std::vector<Task>* tasks);

int main()
{
	bool doingProgram = true;
	std::vector<Task> tasks;

	while (doingProgram)
	{
		std::cout << "Select an option:" << std::endl;

		//Options
		std::cout << "@0: New task" << std::endl;
		std::cout << "@1: Print tasks" << std::endl;
		std::cout << "@2: Delete all tasks" << std::endl;
		std::cout << "@3: Quit" << std::endl;
		
		int selection = 0;
		std::cin >> selection;

		switch (selection)
		{
		default:
			std::cout << "Not a valid selection!" << std::endl;
			//Goes to next while loop iteration
			continue;
			break;
		case 0:
			tasks.push_back(CreateTask());
			break;
		case 1:
			PrintTasks(tasks);
			break;
		case 2:
			DeleteTasks(&tasks);
			break;
		case 3:
			doingProgram = false;
			break;
		}

		system("CLS");
	}

	std::cout << "Terminating program" << std::endl;
	system("PAUSE");
}

Task CreateTask()
{
	//Name entry
	std::cin.ignore();
	std::cout << "Give a task name: " << std::endl;
	std::string name;
	std::getline(std::cin, name);
	std::cout << std::endl;

	//Deadline entry
	std::cout << "Give a deadline: " << std::endl;
	std::string deadline;
	std::getline(std::cin, deadline);
	std::cout << std::endl;

	//Priority entry
	int prio;

	do
	{
		std::cout << "Give a priority (0 = Low, 1 = Normal, 2 = Urgent): ";
		std::cin >> prio;
		std::cout << std::endl;
	}
	while (prio > 2 || prio < 0);

	Task asd(name, deadline, (Task::Priority)prio);
	return asd;
}

void PrintTasks(const std::vector<Task>& tasks)
{
	if (tasks.size() < 1)
	{
		std::cout << "Task list empty!" << std::endl;
	}
	else
	{
		for (int i = 0; i < tasks.size(); i++)
		{
			tasks[i].PrintInfo();
		}
	}
	system("PAUSE");
}

void DeleteTasks(std::vector<Task>* tasks)
{
	tasks->clear();
}